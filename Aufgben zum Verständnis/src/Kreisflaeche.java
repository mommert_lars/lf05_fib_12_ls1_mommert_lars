public class Kreisflaeche {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double flaeche;
		
		flaeche = berechneKreisflaeche(12.0);
		
		System.out.println("Der Flaecheninhat eines Kreises mit einem Radius von 12.0 betr�gt: " + flaeche);
	}
	
	public static double berechneKreisflaeche(double KFR) {
		double Flaecheninhalt;
		
		Flaecheninhalt = Math.PI * (KFR * KFR);
		
		return Flaecheninhalt;
	}
}

/*
	Aufgabe 2: 	In Java werden Parameter an Fuktionen ausschlie�lich als Wert (Call by Value) �bergeben.
				Dabei werden keine neuen Objekte erzeugt, sondern eine Kopie von einem Wert auf diesen Parameter �berschrieben
				
	
	
*/