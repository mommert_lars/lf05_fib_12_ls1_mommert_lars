
public class Lotto {

	public static void main(String[] args) {

		int[] lottoZahlen = new int[6];
		
		lottoZahlen[0] = 3;
		lottoZahlen[1] = 7;
		lottoZahlen[2] = 12;
		lottoZahlen[3] = 18;
		lottoZahlen[4] = 37;
		lottoZahlen[5] = 42;
		
		System.out.printf("[ ");
		
		for(int z = 0; z <= 5; z++) {
			
			System.out.printf(" " + lottoZahlen[z] + " ");
			
		}
		System.out.printf(" ]");		
	}
}
