import java.util.Scanner;

public class Klausur {

	public static void main(String[] args) {
		int[] zahlen= new int[5];
		
		programmhinweis();
		zahlen = eingabe();
		int ergebnis = verarbeitung(zahlen[1], zahlen[2]);
		ausgabe(ergebnis);
		
	}
	
	public static void programmhinweis() {
		System.out.println("Hinweis: ");
		System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
	}

	
	public static int[] eingabe() {
		
		int zahlEins;
		int zahlZwei;
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("1. Zahl: ");
		
		zahlEins = tastatur.nextInt();
		
		System.out.println("2. Zahl: ");
		
		zahlZwei = tastatur.nextInt();
		
		System.out.println("Ihre Eingaben sind " + zahlEins + " und " + zahlZwei + "");
		
		return new int[] {zahlEins, zahlZwei};
	}
	
	public static int verarbeitung(int zahl1, int zahl2) {
		
		int ergebnis = zahl1 * zahl2;
		
		return ergebnis;
		
	}
	
	public static void ausgabe(double ergebnis) {
		
		System.out.println("Das Ergebnis lautet: " + ergebnis + "");
		
	}
}
