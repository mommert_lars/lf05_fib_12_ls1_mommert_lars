
public class Ausgabenformatierung2 {

	public static void main(String[] args) {
		//Aufgabe 1
		System.out.printf("%10s%-10s","*","*");
		System.out.printf("\n%7s%7s","*","*");
		System.out.printf("\n%7s%7s","*","*");
		System.out.printf("\n%10s%-10s","*","*");
		
		//Aufgabe 2
		System.out.printf("\n\n%-5s%-19s=%4d", "0!","=", 1);
		System.out.printf("\n%-5s%-2s%-17d=%4d", "1!","=", 1, 1);
		System.out.printf("\n%-5s%-2s%-17s=%4d", "1!","=", "1 * 2", 2);
		System.out.printf("\n%-5s%-2s%-17s=%4d", "1!","=", "1 * 2 *3", 6);
		System.out.printf("\n%-5s%-2s%-17s=%4d", "1!","=", "1 * 2 * 3 * 4", 24);
		System.out.printf("\n%-5s%-2s%-17s=%4d", "1!","=", "1 * 2 * 3* 4 * 5", 120);
		
	}

}
