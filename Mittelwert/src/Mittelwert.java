
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      double ergebnis;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      ergebnis = berechneMittelwert(x, y);
    
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      System.out.println("Das Ergebnsis lautet: " + ergebnis);
   }

   public static double berechneMittelwert (double x, double y) {
	   double mittelwert;
	   
	   mittelwert = (x + y) / 2.0;
	   
	   return mittelwert;
   }
}
