import java.util.ArrayList;

public class Raumschiff {

	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private static ArrayList<Ladung> Ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>(); 
	
	
	public Raumschiff() {
	}
	
	public Raumschiff(int photonenTorpedoAnzahl, int energieversorgungInProzent, int zustandschildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden, String Schiffsname) {
		this.photonentorpedoAnzahl = photonenTorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandschildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = Schiffsname;
		
	}


	public String getSchiffsname() {
		return schiffsname;
	}


	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}


	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}


	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}


	
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}


	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}


	public int getSchildeInProzent() {
		return schildeInProzent;
	}


	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}


	public int getHuelleInProzent() {
		return huelleInProzent;
	}


	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}


	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}


	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}


	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}


	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}


	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return Ladungsverzeichnis;
	}
/*
	public void setBroadcastKommunikator(String[] broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
*/	
	public static void addLadung(Ladung neueLadung) {
		Ladungsverzeichnis.add(neueLadung);
		}

}
