
public class RaumschiffTEst {
	
	public static void main(String[] args) {

		
		Raumschiff r1 = new Raumschiff(0, 0, 0, 0, 0, 0, "Schiff");
		Ladung l1 = new Ladung("Torpedos", 2);
		r1.addLadung(l1);
		System.out.println(r1);
		
		
		//Raumschiff "klingonen" mit Ladung "ferenigSchneckensaft"
		Ladung ferengiSchneckensaft= new Ladung("Ferengi Schneckensaft", 200);
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		klingonen.addLadung(ferengiSchneckensaft);
		System.out.println(klingonen);
		Ladung klingonenSchwert = new Ladung("Bat'leth Klingonen Schwert", 200);

		
		//Raumschiff "romulaner" mit Ladung "Borg-Schrott" & "Rote Materie"
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		romulaner.addLadung(roteMaterie);
		romulaner.addLadung(borgSchrott);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(plasmaWaffe);
		
		
		//Raumschiff "vulkanier" mit Ladung "Forschungssonde" & "Photonentorpedo"
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100 ,5 , "Ni'Var");
		vulkanier.addLadung(forschungssonde);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(photonentorpedo);
		
		/*
		System.out.println(l1.getMenge());
		System.out.println(l1.getBezeichnung());
*/
//		r1.addLadung(l1);
//		System.out.println(l1.getBezeichnung());
//		System.out.println(l1.getMenge());
		System.out.println(vulkanier);
	}

	public String toString() {
		return "Raumschiff: " + this.raumschiff
	}
}
