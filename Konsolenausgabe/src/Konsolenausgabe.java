
public class Konsolenausgabe {
	
	int Alter = 18;
	String Name = "Lars Mommert";
	
	public static void main(String[] args) {
		
		int Alter = 18;
		String Name = "Lars Mommert";
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		
		System.out.print("Das\t ist \tein \tBeispielsatz.");
		System.out.print("\nEin\t Beispielsatz \tist \tdas.");
		System.out.print("\n \n"+Name+" ist "+Alter+" Jahre alt");
		
		System.out.printf( "\n \n %10s", "*" );
		System.out.printf( "\n %11s", "***" );
		System.out.printf( "\n %12s", "*****");
		System.out.printf( "\n %13s", "*******");
		System.out.printf( "\n %14s", "*********");
		System.out.printf( "\n %15s", "***********");
		System.out.printf( "\n %16s", "*************");
		System.out.printf( "\n %11s", "***" );
		System.out.printf( "\n %11s", "***" );
		
		System.out.printf( "\n|%.2f|" , a);
		System.out.printf( "\n|%.2f|" , b);
		System.out.printf( "\n|%.2f|" , c);
		System.out.printf( "\n|%.2f|" , d);
		System.out.printf( "\n|%.2f|" , e);
		
		
	}
	
	/*
	 * Im Gegensatz zu print() gibt println() etwas mit einem Zeilenumbruch aus, w�hrend print() alles aneinander reiht
	 */

}
