import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		// Benutzereingaben lesen

		String artikel = liesString("Was wollen sie bestellen?: ");
		
		int anzahl = liesInt("Wie viele " + artikel + " wollen sie kaufen?: ");
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");
		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}
	
	public static String liesString(String text) {
		
		System.out.println(text);
		
		Scanner myScanner = new Scanner(System.in);
		
		String artikel = myScanner.next();
		
		return artikel;
	}
	
	public static int liesInt(String text) {
		
		System.out.println(text);
		
		Scanner tastatur = new Scanner(System.in);
		
		int anzahl = tastatur.nextInt();
		
		return anzahl;
	}
	
	public static double liesDouble(String text) {
		
		System.out.println(text);
		
		Scanner tastatur = new Scanner(System.in);
		
		double preis = tastatur.nextDouble();
		
		return preis;
	}
}