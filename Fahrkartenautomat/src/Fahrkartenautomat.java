﻿import java.util.Scanner;

class Fahrkartenautomat
{

	public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double eingezahlterGesamtbetrag;
       double rückgabebetrag;
       double preis = fahrkartenbestellungErfassen();
       double rueckgeld = fahrkartenBezahlen(preis);
       
       fahrkartenAusgeben();
       
       rueckgeldAusgeben(rueckgeld);

       System.out.println("\nWIR WÜNSCHEN IHNEN EINE GUTE FAHRT	^^");
       
    }
	
	//Auswahl Ticktepreis und Anzahl an Tickets
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Bitte geben sie einen Ticketpreis an: ");
	    double ticketpreis = tastatur.nextDouble();
		
	    System.out.print("Wieviele Tickets möchten sie kaufen?: ");
	    int anzahlTickets = tastatur.nextInt();
	    
	    double bestellung = anzahlTickets * ticketpreis;
	    
	    return bestellung;
	   
	}
	
	//Geld-Einwurf
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag = 0.00;
		double eingeworfeneMünze;
		
		
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.print("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " (Euro) \n");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	    
	       
	    double rueckgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag;   
	    
	    return rueckgeld;
	}
	
	//Fahrkarten-Ausgabe
	public static void fahrkartenAusgeben() {
		
		  System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	 
	}
	
	public static void rueckgeldAusgeben(double rueckgeld) {

		
	       if(rueckgeld > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rueckgeld + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rueckgeld >= 2.00) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgeld -= 2.00;
	           }
	           while(rueckgeld >= 1.00) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgeld -= 1.00;
	           }
	           while(rueckgeld >= 0.50) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgeld -= 0.50;
	           }
	           while(rueckgeld >= 0.20) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgeld -= 0.20;
	           }
	           while(rueckgeld >= 0.10) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgeld -= 0.10;
	           }
	           while(rueckgeld >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgeld -= 0.05;
	           }
	       }
	}

}

/*
 * 	5. für die Anzahl von Tickets habe ich Int benutzt, da die Anzahl nur eine ganze Zahl sein kann und keine Nachkommastellen hat. Außerdem ist Int der kleinste mit "tastatur.next" lesbare Datentyp.
 * 	Der Ticketpreis hingegen ist ein double, da der Preis Nachkommastellen haben kann
 * 
 * 	6. Bei anzahl * einzelpreis multipliziert man den bereits eingegebenen Preis für ein Ticket mit der Anzahl an Tickets die man haben will 
 *	Beispiel: Ein Ticket kostet 2,50 € und man will 3 Tickets haben 
 *	2,50€ * 3 = 7,50€
 */

